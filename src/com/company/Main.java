package com.company;
import java.io.IOException;

public class Main {
    public static void main(String[] args){
			float D;
			System.out.println("Решение квадратного уравнения");
					int min = -100;
					int max = 200;
					int a = (int) ((Math.random() * max) - 100);
					int b = (int) ((Math.random() * max) - 100);
					int	c = (int) ((Math.random() * max) - 100);
					D = b * b - 4 * a * c;

					System.out.println("a=" + a);
					System.out.println("b=" + b);
					System.out.println("c=" + c);
					System.out.println("Уравнение имеет вид:" + a + "x^2+" + b + "x+" + c + "=0");
					System.out.println("D=" + D);
					if (D > 0) {
						double x1, x2;
						x1 = (-b - Math.sqrt(D)) / (2 * a);
						x2 = (-b + Math.sqrt(D)) / (2 * a);
						System.out.println("Корни уравнения: x1 = " + x1 + ", x2 = " + x2);
					} else if (D == 0) {
						double x;
						x = -b / (2 * a);
						System.out.println("Уравнение имеет единственный корень: x = " + x);
					} else {
						System.out.println("Уравнение не имеет действительных корней!");
					}
				}
	}